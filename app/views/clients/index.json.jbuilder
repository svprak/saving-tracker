json.array!(@clients) do |client|
  json.extract! client, :id, :first_name, :last_name, :gender, :phone_number, :address
  json.url client_url(client, format: :json)
end
